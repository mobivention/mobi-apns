"""A library for interacting with APNs using HTTP/2 and token based authentication.

"""

__author__ = "Gene Sluder"
__email__ = "gene@mobi.com"
__version__ = "0.1.0"

from .client import APNsClient, APNsMessage
